var Promise = require('es6-promise').Promise;
var gulp = require('gulp'),
	watch = require('gulp-watch'),
	sass = require('gulp-sass'),
	jade = require('gulp-jade'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    prettify = require('gulp-prettify'),
    plumber = require('gulp-plumber'),
    imagemin = require('gulp-imagemin'),
    zip = require('gulp-zip'),
    // del = require('del'),
    // svg_sprite = require('gulp-svg-sprite'),
    png_sprite = require('gulp.spritesmith');
    // cheerio = require('gulp-cheerio'),
    // minify = require('gulp-minify');

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: {
        	pages: 'build/css/',
        	assets: 'build/css/assets'
        },
        img: {
        	assets: 'build/img/assets',
        	icons: 'build/img/icons'
        },
        fonts: 'build/fonts/',
        zip: 'build/..'
    },
    src: { 
    	all: 'src/**/*.*',
    	html: 'src/*.html',
        jade: {
        	assets: 'src/jade/html_assets/*.html',
        	assets_path: 'src/jade/html_assets/',
        	change: 'src/jade/**/*.*',
        	build: 'src/jade/*.jade'
        },
        css: {
        	sprite_path: 'src/style/assets/',
        	assets: 'src/style/assets/*.css',
        	change: 'src/style/**/*.*',
        	build: 'src/style/*.sass'
        },
        img: {
        	change: 'src/img/**/*.*',
        	assets: 'src/img/content/**/*.*',
        	icons: 'src/img/icons/*.*',
        	png_sprite: 'src/img/png_sprite/*.png',
        	svg_sprite: 'src/img/png_sprite/*.svg'

        },
    js: 'src/js/**/*.js',
    fonts: 'src/fonts/*.*',
    zip: 'build/**/*.*'
    }   
};

gulp.task('default', ['build', 'watch']);
gulp.task('build', ['html', 'jade', 'images', 'fonts', 'style', 'js', 'server_reload', 'zip']);

gulp.task('html', function(){
	return gulp.src(path.src.html)
        .pipe(prettify())
       	.pipe(gulp.dest(path.build.html))
       	.on('end', browserSync.reload);
});

gulp.task('jade', function(){
	gulp.src(path.src.jade.assets)
		.pipe(prettify())
		.pipe(gulp.dest(path.src.jade.assets_path));
	return gulp.src(path.src.jade.build)
		.pipe(plumber())
			.pipe(jade({pretty: true}))
	    .pipe(plumber.stop())
			.pipe(gulp.dest(path.build.html))
			.on('end', browserSync.reload);
});

gulp.task('style', function(){
	gulp.src(path.src.css.assets)
		.pipe(gulp.dest(path.build.css.assets));
	return gulp.src(path.src.css.build)
		.pipe(plumber())
     		.pipe(sass())
      	.pipe(plumber.stop()) 
      		.pipe(autoprefixer())    
      		.pipe(gulp.dest(path.build.css.pages))
  			.on('end', browserSync.reload);
});

gulp.task('images', function(){
	gulp.src(path.src.img.assets)
    	.pipe(imagemin())
    	.pipe(gulp.dest(path.build.img.assets))
    gulp.src(path.src.img.png_sprite)
    	.pipe(png_sprite({
    		imgName: 'sprite.png',
    		cssName: 'png_sprite.sass',
    		imgPath: '../img/icons/'
    		}))
    	.pipe(gulp.dest(path.build.img.icons))
    	.on('end', browserSync.reload);
});

gulp.task('fonts', function(){
  return gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.build.fonts));
});

gulp.task('js', function() {
    gulp.src(path.src.js)
        // .pipe(minify())      
        .pipe(gulp.dest(path.build.js))
        .on('end', browserSync.reload);
});

gulp.task('zip', function(){
    var date = new Date();
    gulp.src(path.src.zip)
        .pipe(zip("build.zip"))
        .pipe(gulp.dest(path.build.zip));
});
// date.getDate() + "_" + date.getMonth() + "_" + date.getFullYear()
gulp.task('webserver', function() {
    browserSync.init({
      server: {
        baseDir: path.build.html,
        index: "index.html"
        },
      // tunnel: true,
      ghostMode: false,
      notify: false,
      open: false
    });
});

gulp.task('server_reload', function() {
	browserSync.reload;
});

gulp.task('watch', ['webserver'],function(){
	gulp.watch(path.src.html, ['html']),
	gulp.watch(path.src.img.change, ['images']);
	gulp.watch(path.src.fonts, ['fonts']);
	gulp.watch(path.src.css.change, ['style']);
	gulp.watch(path.src.js, ['js']);
	gulp.watch(path.src.jade.change, ['jade']);       
});
